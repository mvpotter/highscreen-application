package org.highscreen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * RuntimeException created for error processing.<br />
 * <br />
 * Instance has an error code which allows to convert it to user friendly message.<br />
 * Module should have org/highscreen/errors/*.properties file with the name of a Class
 * that implements ErrorMessages interface.<br />
 * All messages related to error codes must be described there. Thus, they will be returned by <b>getErrorMessage</b>
 * method call.
 */
public class SystemException extends RuntimeException {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemException.class);

    private final ErrorCode errorCode;

    public SystemException(final ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return getErrorMessage(Locale.getDefault());
    }

    public String getErrorMessage(final Locale locale) {
        String message = "";
        if (errorCode == null) {
            return message;
        }
        final String key = String.valueOf(errorCode.getNumber());
        ResourceBundle bundle;
        final String bundlePath = errorCode.getClass().getName();
        try {
            bundle = ResourceBundle.getBundle(bundlePath, locale);
            message = bundle.getString(key);
        } catch (MissingResourceException exception) {
            LOGGER.error(bundlePath + " not found", exception);
        }

        return message;
    }

}
