package org.highscreen;

/**
 * Represents error with code in highscreen application.
 */
public interface ErrorCode {

    int getNumber();

}
