highscreen-application
=======================

Software for reading, converting and displaying GEO-information from video register Highscreen Black Box Radar and analogous.

The main goal of the project is to create desktop application for displaying video and GEO-information from video register Highscreen Black Box Radar and its analogous.
Unfortunately, the company does not released any version of player for Mac OS X. Moreover, the support team keeps silence when I ask them if they have intention to create it in the near future.
Thus, I decided to create multiplutform application for displaying this information.

Done:

  -  Decoder for *.dat files
  -  Converter to CSV format
  -  Graphical interface for converter
  -  Executable JAR assemply for converter

Further development plans:
  
  -  Converter to KML format
  -  Google Map representation
