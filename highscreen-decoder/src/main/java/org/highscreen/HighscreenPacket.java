package org.highscreen;

import org.nmea.gprmc.GprmcPacket;

/**
 * Highscreen packet.<br />
 * Consists of GPRMC packet and G-sensor data
 */
public class HighscreenPacket {

    public static final String DELIMITER = "\t";

    public static final int G_SENSOR_X_INDEX = 0;
    public static final int G_SENSOR_Y_INDEX = 1;
    public static final int G_SENSOR_Z_INDEX = 2;
    public static final int GPRMC_DATA_INDEX = 3;

    private GSensorData gSensorData;
    private GprmcPacket gprmcPacket;

    public HighscreenPacket(final GSensorData gSensorData, final GprmcPacket gprmcPacket) {
        this.gSensorData = gSensorData;
        this.gprmcPacket = gprmcPacket;
    }

    public GSensorData getgSensorData() {
        return gSensorData;
    }

    public void setgSensorData(final GSensorData gSensorData) {
        this.gSensorData = gSensorData;
    }

    public GprmcPacket getGprmcPacket() {
        return gprmcPacket;
    }

    public void setGprmcPacket(final GprmcPacket gprmcPacket) {
        this.gprmcPacket = gprmcPacket;
    }

}
