package org.highscreen.validation;

import org.nmea.gprmc.validation.Validator;

import static org.highscreen.GSensorData.*;

/**
 * Validator gor G-sensor data.
 */
public class GSensorDataValidator implements Validator {

    public static final int NUMBER_OF_G_SENSOR_TOKENS = 3;

    @Override
    public boolean validate(final String value) {
        final String[] tokensArray = value.split(DELIMITER);
        return tokensArray.length == NUMBER_OF_G_SENSOR_TOKENS
               && validateGSensorAxis(tokensArray[X_AXIS_INDEX])
               && validateGSensorAxis(tokensArray[Y_AXIS_INDEX])
               && validateGSensorAxis(tokensArray[Z_AXIS_INDEX]);

    }

    private boolean validateGSensorAxis(final String gSensorComponent) {
        try {
            Integer.parseInt(gSensorComponent);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

}
