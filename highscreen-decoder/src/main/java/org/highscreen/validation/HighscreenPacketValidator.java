package org.highscreen.validation;

import org.highscreen.HighscreenPacket;
import org.nmea.gprmc.validation.GprmcValidator;
import org.nmea.gprmc.validation.Validator;

/**
 * Validator for highscreen packet data.
 */
public class HighscreenPacketValidator implements Validator {

    public static final int HIGHSCREEN_PACKET_TOKENS_NUMBER = 4;
    private Validator gSensorDataValidator = new GSensorDataValidator();
    private Validator gprmcValidator = new GprmcValidator();

    @Override
    public boolean validate(final String value) {
        final String[] tokensArray = value.split(HighscreenPacket.DELIMITER);

        if (tokensArray.length != HIGHSCREEN_PACKET_TOKENS_NUMBER) {
            return false;
        }
        final int separatorIndex = value.lastIndexOf(HighscreenPacket.DELIMITER);
        if (!gSensorDataValidator.validate(value.substring(0, separatorIndex))) {
            return false;
        }

        return gprmcValidator.validate(value.substring(separatorIndex + 1));
    }

}
