package org.highscreen.decoder;

import java.io.File;
import java.io.InputStream;

/**
 * File decoder.
 */
public interface Decoder {

    String[] decode(final File file);
    String[] decode(final InputStream inputStream);

}
