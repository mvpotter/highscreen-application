package org.highscreen.decoder;

import org.apache.commons.io.IOUtils;
import org.nmea.gprmc.GprmcPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Decodes highscreen files with GEO-information.<br />
 * The result of decoding is string in GPRMC format extended with G-sensor data.
 */
public class HighscreenDatFileDecoder implements Decoder {

    public static final int CHECKSUM_LENGTH = 3;
    private static final Logger LOGGER = LoggerFactory.getLogger(HighscreenDatFileDecoder.class);

    /**
     * Decodes file to GPRMC format extended with G-sensor data.
     *
     * @param file file to decode
     * @return decoded GPRMC extended with G-sensor data
     */
    @Override
    public String[] decode(final File file) {
        String[] lines = new String[0];
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
            lines = decode(fileInputStream);
            IOUtils.closeQuietly(fileInputStream);
        } catch (FileNotFoundException e) {
            LOGGER.warn(e.getMessage());
        }

        return lines;
    }

    /**
     * Decodes input stream to GPRMC format extended with G-sensor data.
     *
     * @param inputStream input stream to be decoded
     * @return decoded GPRMC string extended with G-sensor data
     */
    public String[] decode(final InputStream inputStream) {
        final List<String> lines = new LinkedList<String>();
        try {
            Integer[] line;
            while ((line = readLine(inputStream)) != null) {
                final String decodedLine = decodeLine(line);
                if (decodedLine != null) {
                    LOGGER.debug(decodedLine);
                    lines.add(decodedLine);
                }
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return lines.toArray(new String[0]);
    }

    private String decodeLine(final Integer[] line) {
        final int offset = GprmcPacket.CHECKSUM_DELIMITER - line[line.length - CHECKSUM_LENGTH];
        final StringBuilder stringBuilder = new StringBuilder();
        for (Integer character : line) {
            stringBuilder.append((char) (character + offset));
        }
        // check that line is decoded correctly and has appropriate format
        final String result = stringBuilder.toString();
        if (!result.contains(GprmcPacket.GPRMC_KEY)) {
            return null;
        } else {
            return result;
        }
    }

    private Integer[] readLine(final InputStream inputStream) throws IOException {
        final List<Integer> characters = new LinkedList<Integer>();
        int currentChar = inputStream.read();
        while (currentChar != '\r' && currentChar != '\n' && currentChar != -1) {
            characters.add(currentChar);
            currentChar = inputStream.read();
        }

        if (currentChar == '\r') {
            inputStream.read();
        }

        // The end of file is reached
        if (currentChar == -1 && characters.isEmpty()) {
            return null;
        }

        return characters.toArray(new Integer[0]);
    }

}
