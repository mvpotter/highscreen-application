package org.highscreen.parser;

import org.highscreen.GSensorData;
import org.highscreen.HighscreenPacket;
import org.highscreen.SystemException;
import org.highscreen.validation.HighscreenPacketValidator;
import org.nmea.Parser;
import org.nmea.gprmc.GprmcPacket;
import org.nmea.gprmc.GprmcParser;
import org.nmea.gprmc.errors.ParserError;
import org.nmea.gprmc.validation.Validator;

import static org.highscreen.HighscreenPacket.*;

/**
 * Parser for highscreen packet.
 */
public class HighscreenPacketParser implements Parser<HighscreenPacket> {

    private Validator validator = new HighscreenPacketValidator();
    private Parser<GprmcPacket> parser = new GprmcParser();

    @Override
    public HighscreenPacket parse(final String string) {
        if (!validator.validate(string)) {
            throw new SystemException(ParserError.UNSUPPORTED_FORMAT);
        }
        final String[] tokensArray = string.split(DELIMITER);
        final GSensorData gSensorData = new GSensorData(Integer.parseInt(tokensArray[G_SENSOR_X_INDEX]),
                                                        Integer.parseInt(tokensArray[G_SENSOR_Y_INDEX]),
                                                        Integer.parseInt(tokensArray[G_SENSOR_Z_INDEX]));
        final GprmcPacket gprmcPacket = parser.parse(tokensArray[GPRMC_DATA_INDEX]);

        return new HighscreenPacket(gSensorData, gprmcPacket);
    }

}
