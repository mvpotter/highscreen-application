package org.highscreen;

/**
 * Data from D-sensor.
 */
public class GSensorData {

    public static final String DELIMITER = "\t";

    public static final int X_AXIS_INDEX = 0;
    public static final int Y_AXIS_INDEX = 0;
    public static final int Z_AXIS_INDEX = 0;

    private int x;
    private int y;
    private int z;

    public GSensorData(final int x, final int y, final int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public void setX(final int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(final int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(final int z) {
        this.z = z;
    }

}
