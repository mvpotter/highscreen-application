package org.highscreen.decoder;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class HighscreenDatFileDecoderTest {

    private static final String TEST_INPUT_FILE_NAME = "/org/highscreen/decoder/test_input.dat";
    private static final String TEST_OUTPUT_FILE_NAME = "/org/highscreen/decoder/test_output.dat";

    private Decoder decoder = new HighscreenDatFileDecoder();

    @Test
    public void testDecodeFile() throws IOException {
        String inputFile = getClass().getResource(TEST_INPUT_FILE_NAME).getFile();
        String[] output = decoder.decode(new File(inputFile));
        Assert.assertTrue(output.length > 0);

        String outputFileName = getClass().getResource(TEST_OUTPUT_FILE_NAME).getFile();
        String[] expectedOutput = FileUtils.readFileToString(new File(outputFileName)).split("\n");

        Assert.assertArrayEquals(expectedOutput, output);
    }

    @Test
    public void testDecodeUnexistentFile() {
        String[] output = decoder.decode(new File(""));
        Assert.assertTrue(output.length == 0);
    }

}
