package org.highscreen.parser;

import org.highscreen.HighscreenPacket;
import org.junit.Assert;
import org.junit.Test;
import org.nmea.Parser;

public class HighscreenParserTest {

    private static final String CORRECT_TEST_STRING = "-15\t62\t-1312\t$GPRMC,003640.000,A,5451.4934,N,08305.7646,E,23.42,319.70,090513,,,A*58";

    private Parser<HighscreenPacket> parser = new HighscreenPacketParser();

    @Test
    public void testParse() {
        HighscreenPacket packet = parser.parse(CORRECT_TEST_STRING);
        Assert.assertNotNull(packet);
    }

}
