package org.highscreen.errors;

import org.highscreen.ErrorCode;

/**
 * Describes converter errors.
 */
public enum ConverterError implements ErrorCode {
    ABSENT_SOURCE(1),
    NULL_DESTINATION(2),
    SOURCE_DESTINATION_TYPES(3),
    NOTHING_TO_CONVERT(4),
    CANNOT_CREATE_FILE(5);

    private int errorCode;

    private ConverterError(final int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public int getNumber() {
        return errorCode;
    }
}
