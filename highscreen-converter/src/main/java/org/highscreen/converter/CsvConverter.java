package org.highscreen.converter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.highscreen.HighscreenPacket;
import org.highscreen.SystemException;
import org.highscreen.decoder.Decoder;
import org.highscreen.decoder.HighscreenDatFileDecoder;
import org.highscreen.errors.ConverterError;
import org.highscreen.parser.HighscreenPacketParser;
import org.nmea.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Locale;
import java.util.Observable;

/**
 * Converts highscreen data to csv file with header "Time,Latitude,Longitude,Speed(km/h),Course,GX,GY,GZ".
 */
public class CsvConverter extends Observable implements Converter {

    public static final String CSV_EXTENSION = "csv";
    public static final String DAT_EXTENSION = "dat";
    public static final int MAX_PROGRESS = 100;

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvConverter.class);

    private static final String DIRECTORY_CREATION_ERROR_FORMAT = "Unable to create directory: %s";
    private static final String DELETITION_ERROR_FORMAT = "Unable to create delete: %s";

    private static final String DELIMITER = ",";
    private static final String LINE_BREAK = "\n";

    private static final String CSV_HEADER = "Time,Latitude,Longitude,Speed(km/h),Course,GX,GY,GZ";

    private static final double KNOTS_IN_KMPH = 1.852;

    private static final Decoder DECODER = new HighscreenDatFileDecoder();
    private static final Parser<HighscreenPacket> PARSER = new HighscreenPacketParser();

    private boolean directoryToFile;
    private boolean firstFile = true;
    private int progress;

    /**
     * Converts highscreen data to csv file with header "Time,Latitude,Longitude,Speed(km/h),Course".
     *
     * @param source      source file
     * @param destination converted file
     */
    @Override
    public void convert(final File source, final File destination) {
        setProgress(0);
        validateInputParameters(source, destination);
        createFileIfNotExists(destination);
        // If all files from directory should be converted in a single file
        if (source.isDirectory() && destination.isFile()) {
            directoryToFile = true;
            // Delete destination file
            if (destination.exists() && !destination.delete()) {
                LOGGER.error(String.format(DELETITION_ERROR_FORMAT, destination.getAbsolutePath()));
            }
            createFileIfNotExists(destination);
        }

        if (source.isDirectory()) {
            processDirectory(source, destination);
        } else {
            processFile(source, destination);
        }
        setProgress(MAX_PROGRESS);
        firstFile = true;
    }


    private void validateInputParameters(final File source, final File destination) {
        // Check input parameters
        if (source == null || !source.exists()) {
            throw new SystemException(ConverterError.ABSENT_SOURCE);
        }
        if (destination == null) {
            throw new SystemException(ConverterError.NULL_DESTINATION);
        }
        if (source.isFile() && destination.isDirectory()) {
            throw new SystemException(ConverterError.SOURCE_DESTINATION_TYPES);
        }
    }

    private void createFileIfNotExists(final File file) {
        if (!file.exists()) {
            try {
                if (FilenameUtils.getExtension(file.getAbsolutePath()).isEmpty()) {
                    if (!file.mkdirs()) {
                        LOGGER.error(String.format(DIRECTORY_CREATION_ERROR_FORMAT, file.getParent()));
                    }
                } else {
                    if (file.getParentFile() != null && !file.getParentFile().exists()
                            && !file.getParentFile().mkdirs()) {
                        LOGGER.error(String.format(DIRECTORY_CREATION_ERROR_FORMAT, file.getParent()));
                    }
                    if (!file.createNewFile()) {
                        LOGGER.warn("The file is already exists:" + file.getAbsolutePath());
                    }
                }
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
                throw new SystemException(ConverterError.CANNOT_CREATE_FILE);
            }
        }
    }

    private void processDirectory(final File source, final File destination) {
        final Collection<File> files = FileUtils.listFiles(source, new String[]{DAT_EXTENSION}, true);
        final int filesCount = files.size();
        int i = 0;
        for (File file : files) {
            File finalDestination = destination;
            if (!directoryToFile) {
                finalDestination = new File(destination,
                        FilenameUtils.getBaseName(file.getName()) + "." + CSV_EXTENSION);
            }
            processFile(file, finalDestination);
            i += 1;
            setProgress((int) ((i / (float) filesCount) * MAX_PROGRESS));
        }
    }

    private void setProgress(final int progress) {
        if (this.progress != progress) {
            this.progress = progress;
            setChanged();
            notifyObservers(progress);
        }
    }

    private void processFile(final File source, final File destination) {
        final String[] decodedLines = DECODER.decode(source);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(destination, true);
            if (!directoryToFile || firstFile) {
                IOUtils.write(CSV_HEADER + LINE_BREAK, outputStream);
                firstFile = false;
            }
            int i = 0;
            for (String line : decodedLines) {
                try {
                    final HighscreenPacket highscreenPacket = PARSER.parse(line);
                    LOGGER.debug("Writing: " + createCsvString(highscreenPacket));
                    IOUtils.write(createCsvString(highscreenPacket), outputStream);
                    i += 1;
                } catch (SystemException exception) {
                    LOGGER.warn("Line cannot be parsed: " + line);
                    LOGGER.debug(exception.getErrorMessage(), exception.getErrorCode().getNumber());
                }
            }
            outputStream.flush();
            if (!directoryToFile && i == 0 && !destination.delete()) {
                LOGGER.error(String.format(DELETITION_ERROR_FORMAT, destination.getAbsolutePath()));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
    }

    private String createCsvString(final HighscreenPacket highscreenPacket) {
        final DateFormat timeStampFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        final String doubleFormat = "%.4f";
        final StringBuilder builder = new StringBuilder();
        builder.append(timeStampFormat.format(highscreenPacket.getGprmcPacket().getTimeStamp())).append(DELIMITER);
        final double latitude = highscreenPacket.getGprmcPacket().getGprmcLatitude().getValue();
        builder.append(String.format(Locale.ENGLISH, doubleFormat, latitude)).append(DELIMITER);
        final double longitude = highscreenPacket.getGprmcPacket().getGprmcLongitude().getValue();
        builder.append(String.format(Locale.ENGLISH, doubleFormat, longitude)).append(DELIMITER);
        final double speed = highscreenPacket.getGprmcPacket().getSpeed() * KNOTS_IN_KMPH;
        builder.append(String.format(Locale.ENGLISH, "%.2f", speed)).append(DELIMITER);
        builder.append(highscreenPacket.getGprmcPacket().getCourse()).append(DELIMITER);
        builder.append(highscreenPacket.getgSensorData().getX()).append(DELIMITER);
        builder.append(highscreenPacket.getgSensorData().getY()).append(DELIMITER);
        builder.append(highscreenPacket.getgSensorData().getZ());
        builder.append(LINE_BREAK);
        return builder.toString();
    }

}
