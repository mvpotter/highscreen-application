package org.highscreen.converter.application;

/**
 * Main entrance to launch application.
 */
public final class Main {

    private Main() {

    }

    public static void main(final String[] args) {
        if (args.length != 0) {
            new ConsoleApplication().launch(args);
        } else {
            new DesktopApplication().launch();
        }
    }

}
