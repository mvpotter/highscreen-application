package org.highscreen.converter.application;

import org.highscreen.converter.application.gui.ConverterFrame;

/**
 * Desktop application launcher.
 */
public class DesktopApplication {

    public void launch() {
        final ConverterFrame converterFrame = new ConverterFrame();
        converterFrame.setVisible(true);
    }

}
