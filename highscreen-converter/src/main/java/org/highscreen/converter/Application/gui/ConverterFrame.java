package org.highscreen.converter.application.gui;

import org.apache.commons.io.FilenameUtils;
import org.highscreen.SystemException;
import org.highscreen.converter.Converter;
import org.highscreen.converter.CsvConverter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 * Main frame of desktop application.
 */
public class ConverterFrame extends JFrame implements Observer {

    public static final int PROGRESS_BAR_LENGTH = 100;
    private static final int PADDING = 5;

    private ResourceBundle bundle;
    private JProgressBar progressBar;

    public ConverterFrame() {
        super();
        bundle = ResourceBundle.getBundle("org.highscreen.messages.messages");
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setTitle(bundle.getString("title"));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        final SpringLayout layoutManager = new SpringLayout();
        getContentPane().setLayout(layoutManager);

        addControls(layoutManager);

        this.pack();
        this.setLocation((int) ((screenSize.width - this.getSize().getWidth()) / 2.0),
                (int) ((screenSize.height - this.getSize().getHeight()) / 2.0));
    }

    private void addControls(final SpringLayout layoutManager) {
        final JTextField sourcePath = new JTextField(20);
        sourcePath.setEditable(false);

        final JButton sourceButton = new JButton(bundle.getString("source_button"));
        final JTextField destPath = new JTextField(20);
        destPath.setEditable(false);
        final JButton destButton = new JButton(bundle.getString("dest_button"));
        destButton.setEnabled(false);
        final JButton convertButton = new JButton(bundle.getString("convert_button"));
        convertButton.setEnabled(false);

        sourceButton.addActionListener(new SourceButtonActionListener(sourcePath, destPath, destButton, convertButton));
        destButton.addActionListener(new DestButtonActionListener(destPath));
        convertButton.addActionListener(new ConvertButtonActionListener(convertButton, sourcePath, destPath));

        progressBar = new JProgressBar(0 , PROGRESS_BAR_LENGTH);
        progressBar.setStringPainted(true);

        this.getContentPane().add(sourcePath);
        this.getContentPane().add(sourceButton);
        this.getContentPane().add(destPath);
        this.getContentPane().add(destButton);
        this.getContentPane().add(progressBar);
        this.getContentPane().add(convertButton);

        setPositionConstraints(layoutManager, sourcePath, sourceButton, destPath, destButton, convertButton);
    }

    private void setPositionConstraints(final SpringLayout layoutManager,
                                        final JTextField sourcePath, final JButton sourceButton,
                                        final JTextField destPath, final JButton destButton,
                                        final JButton convertButton) {
        layoutManager.putConstraint(SpringLayout.NORTH, sourcePath, PADDING,
                                    SpringLayout.NORTH, getContentPane());
        layoutManager.putConstraint(SpringLayout.WEST, sourcePath, PADDING,
                                    SpringLayout.WEST, getContentPane());

        layoutManager.putConstraint(SpringLayout.NORTH, sourceButton, PADDING,
                                    SpringLayout.NORTH, getContentPane());
        layoutManager.putConstraint(SpringLayout.WEST, sourceButton, PADDING,
                                    SpringLayout.EAST, sourcePath);

        layoutManager.putConstraint(SpringLayout.NORTH, destPath, PADDING,
                                    SpringLayout.SOUTH, sourcePath);
        layoutManager.putConstraint(SpringLayout.WEST, destPath, PADDING,
                                    SpringLayout.WEST, getContentPane());

        layoutManager.putConstraint(SpringLayout.WEST, destButton, PADDING,
                                    SpringLayout.EAST, destPath);
        layoutManager.putConstraint(SpringLayout.NORTH, destButton, PADDING,
                                    SpringLayout.SOUTH, sourceButton);

        // FIXME: why does progress bar has different paddings in comparison with other components?
        layoutManager.putConstraint(SpringLayout.EAST, progressBar, -PADDING * 2,
                                    SpringLayout.EAST, getContentPane());
        layoutManager.putConstraint(SpringLayout.WEST, progressBar, PADDING * 2,
                                    SpringLayout.WEST, getContentPane());
        layoutManager.putConstraint(SpringLayout.NORTH, progressBar, PADDING * 2,
                                    SpringLayout.SOUTH, destPath);

        layoutManager.putConstraint(SpringLayout.EAST, convertButton, -PADDING,
                                    SpringLayout.EAST, getContentPane());
        layoutManager.putConstraint(SpringLayout.NORTH, convertButton, PADDING * 2,
                                    SpringLayout.SOUTH, progressBar);

        layoutManager.putConstraint(SpringLayout.SOUTH, getContentPane(), PADDING,
                                    SpringLayout.SOUTH, convertButton);

        final JButton widestButton = sourceButton.getText().length() >= destButton.getText().length()
                                     ? sourceButton : destButton;
        layoutManager.putConstraint(SpringLayout.EAST, getContentPane(), PADDING,
                                    SpringLayout.EAST, widestButton);
    }

    @Override
    public void update(final Observable observable, final Object o) {
        final int progress = (Integer) o;
        progressBar.setValue(progress);
    }

    private void showErrorMessage(final SystemException exception) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JOptionPane.showMessageDialog(ConverterFrame.this, exception.getErrorMessage(),
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    /**
     * Action listener for source button.
     */
    private final class SourceButtonActionListener implements ActionListener {

        private JTextField sourcePath;
        private JTextField destPath;
        private JButton destButton;
        private JButton convertButton;

        private SourceButtonActionListener(final JTextField sourcePath, final JTextField destPath,
                                           final JButton destButton, final JButton convertButton) {
            this.sourcePath = sourcePath;
            this.destPath = destPath;
            this.destButton = destButton;
            this.convertButton = convertButton;
        }

        @Override
        public void actionPerformed(final ActionEvent actionEvent) {
            final JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            final int result = fileChooser.showOpenDialog(ConverterFrame.this);
            if (result == JFileChooser.APPROVE_OPTION) {
                final String selectedPath = fileChooser.getSelectedFile().getAbsolutePath();
                sourcePath.setText(selectedPath);
                sourcePath.setCaretPosition(sourcePath.getDocument().getLength());
                destButton.setEnabled(true);
                String destinationName;
                if (fileChooser.getSelectedFile().isDirectory()) {
                    destinationName = selectedPath + "_" + CsvConverter.CSV_EXTENSION;
                } else {
                    destinationName = FilenameUtils.removeExtension(selectedPath) + "." + CsvConverter.CSV_EXTENSION;
                }
                destPath.setText(destinationName);
                convertButton.setEnabled(true);
            }
        }
    }

    /**
     * Action listener for destination button.
     */
    private final class DestButtonActionListener implements ActionListener {

        private JTextField destPath;

        private DestButtonActionListener(final JTextField destPath) {
            this.destPath = destPath;
        }

        @Override
        public void actionPerformed(final ActionEvent actionEvent) {
            final JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            final int result = fileChooser.showSaveDialog(ConverterFrame.this);
            if (result == JFileChooser.APPROVE_OPTION) {
                destPath.setText(fileChooser.getSelectedFile().getAbsolutePath());
                destPath.setCaretPosition(destPath.getDocument().getLength());
                System.out.println(fileChooser.getSelectedFile().getName());
            }
        }
    }

    /**
     * Action listener for convert button.
     */
    private final class ConvertButtonActionListener implements ActionListener {

        private JButton convertButton;
        private JTextField sourcePath;
        private JTextField destPath;

        private ConvertButtonActionListener(final JButton convertButton, final JTextField sourcePath,
                                            final JTextField destPath) {
            this.convertButton = convertButton;
            this.sourcePath = sourcePath;
            this.destPath = destPath;
        }

        @Override
        public void actionPerformed(final ActionEvent actionEvent) {
            convertButton.setEnabled(false);
            final Converter converter = new CsvConverter();
            ((Observable) converter).addObserver(ConverterFrame.this);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        converter.convert(new File(sourcePath.getText()), new File(destPath.getText()));
                    } catch (final SystemException exception) {
                        showErrorMessage(exception);
                    } finally {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                convertButton.setEnabled(true);
                            }
                        });
                    }
                }
            });
            thread.start();
        }
    }

}
