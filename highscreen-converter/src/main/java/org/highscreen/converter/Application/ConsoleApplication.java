package org.highscreen.converter.application;

import org.apache.commons.io.FilenameUtils;
import org.highscreen.SystemException;
import org.highscreen.converter.Converter;
import org.highscreen.converter.CsvConverter;
import org.highscreen.errors.ConverterError;

import java.io.File;

/**
 * Console application launcher.
 */
public class ConsoleApplication {

    private Converter converter = new CsvConverter();

    public void launch(final String[] launchArguments) {
        if (launchArguments.length == 0) {
            printUsage();
            System.exit(0);
        }

        final File sourceFile = getSource(launchArguments);

        if (!sourceFile.exists()) {
            throw new SystemException(ConverterError.ABSENT_SOURCE);
        }

        final File destinationFile = getDestination(launchArguments);

        converter.convert(sourceFile, destinationFile);

        System.out.println("The file is converted successfully");
    }

    private File getSource(final String[] launchArguments) {
        File sourceFile = null;
        if (launchArguments.length != 0) {
            sourceFile = new File(launchArguments[0]);
        }

        return sourceFile;
    }

    private File getDestination(final String[] launchArguments) {
        File destinationFile = null;

        if (launchArguments.length == 2) {
            destinationFile = new File(launchArguments[1]);
            createDestinationPath(destinationFile);
        } else if (launchArguments.length == 1) {
            final File sourceFile = new File(launchArguments[0]);
            if (sourceFile.isFile()) {
                destinationFile = new File(FilenameUtils.getBaseName(sourceFile.getName()) + "."
                                           + CsvConverter.CSV_EXTENSION);
            } else {
                destinationFile = new File(sourceFile.getParentFile(), sourceFile.getName() + "_"
                                           + CsvConverter.CSV_EXTENSION);
            }
            createDestinationPath(destinationFile);
        }

        return destinationFile;
    }

    private void createDestinationPath(final File destinationFile) {
        File pathToCreate;
        if (FilenameUtils.getExtension(destinationFile.getName()).isEmpty()) {
            pathToCreate = destinationFile;
        } else {
            pathToCreate = destinationFile.getParentFile();
        }
        if (!pathToCreate.exists() && !pathToCreate.mkdirs()) {
            System.out.println("Unable to create destination path " + pathToCreate.getAbsolutePath()
                               + ". Please check that you have appropriate permissions.");
            System.exit(0);
        }
    }

    public void printUsage() {
        System.out.println("Usage: java -jar highscreen-converter.jar <source> [<destination>]");
        System.out.println();
        System.out.println("Converts highscreen *.dat files to CSV format");
        System.out.println();
        System.out.println("<source> - highscreen .dat file or directory that contains files");
        System.out.println("<destination> - the file or directory where the result will be saved");
        System.out.println();
        System.out.println("If destination is not defined the result will be saved in "
                           + "the folder with the source file/folder.");
    }

}
