package org.highscreen.converter;

import java.io.File;

/**
 * Converter interface.
 */
public interface Converter {

    /**
     * Converts source file and saves the result to destination.
     *
     * @param source source file
     * @param destination converted file
     *
     * @throws SystemException if error occurs in converting process
     */
    void convert(File source, File destination);

}
