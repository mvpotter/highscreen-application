package org.highscreen.converter;

import org.apache.commons.io.FileUtils;
import org.highscreen.SystemException;
import org.highscreen.errors.ConverterError;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class CsvConverterTest {

    private static final String TEST_INPUT_FILE_NAME = "/org/highscreen/converter/test_input.dat";
    private static final String TEST_OUTPUT_FILE_NAME = "/org/highscreen/converter/test_output.csv";

    private Converter converter = new CsvConverter();

    private File inputFile;
    private File outputFile;
    private String expectedOutput;

    @Before
    public void before() throws IOException {
        String inputFileName = getClass().getResource(TEST_INPUT_FILE_NAME).getFile();
        String outputFileName = getClass().getResource(TEST_OUTPUT_FILE_NAME).getFile();
        outputFile = File.createTempFile("highscreen_test_data", ".csv");
        inputFile = new File(inputFileName);
        expectedOutput = FileUtils.readFileToString(new File(outputFileName));
    }

    @Test
    public void testConvert() throws IOException {
        converter.convert(inputFile, outputFile);
        String output = FileUtils.readFileToString(outputFile);
        Assert.assertEquals(expectedOutput, output);
    }

    @Test
    public void testAbsentSource() {
        try {
            converter.convert(null, null);
        } catch (SystemException exception) {
            Assert.assertEquals(ConverterError.ABSENT_SOURCE, exception.getErrorCode());
        }
    }

    @After
    public void after() {
        if (outputFile != null) {
            outputFile.delete();
        }
    }

}
