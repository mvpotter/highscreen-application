package org.nmea;

/**
 * Parser interface.
 *
 * @param <T> returned type.
 */
public interface Parser<T> {

    T parse(String string);

}
