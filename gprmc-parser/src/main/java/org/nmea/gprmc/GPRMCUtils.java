package org.nmea.gprmc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utility class to work with GPRMC data.
 */
public final class GprmcUtils {

    public static final int TIME_STRING_LENGTH = 6;

    private static final Logger LOGGER = LoggerFactory.getLogger(GprmcUtils.class);

    private GprmcUtils() {

    }

    /**
     * Returns time stamp of GPRMC packet.
     *
     * @param tokensArray array of splitted by comma GPRMC string
     * @return GPRMC packet timestamp
     */
    public static Date getTimeStamp(final String[] tokensArray) {
        final DateFormat dateFormat = new SimpleDateFormat(GprmcPacket.DATE_TIME_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String date = tokensArray[GprmcPacket.DATE_INDEX]
                          + tokensArray[GprmcPacket.TIME_INDEX].substring(0, TIME_STRING_LENGTH);
        Date result = null;
        try {
            result = dateFormat.parse(date);
        } catch (ParseException e) {
            LOGGER.error("Unable to parse date: " + date);
        }

        return result;
    }

}
