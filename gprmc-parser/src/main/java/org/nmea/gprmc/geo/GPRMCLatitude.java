package org.nmea.gprmc.geo;

/**
 * Latitude for using in GPRMC format.
 */
public class GprmcLatitude {

    public static final double MINUTES_IN_DEGREE = 60.0;
    /**
     * GPRMC latitude value in "ddmm.mm" format.<br />
     * d - degrees<br />
     * m - minutes
     */
    private String gprmcValue;

    /**
     * Absolute value.<br />
     * > 0 -> North<br />
     * < 0 -> South
     */
    private double value;

    /**
     * Latitude type.<br />
     * <b>N</b> - north<br />
     * <b>S</b> - south
     */
    private Type type;

    public GprmcLatitude(final String gprmcValue, final Type type) {
        setType(type);
        setGprmcValue(gprmcValue);
    }

    public String getGprmcValue() {
        return gprmcValue;
    }

    public double getValue() {
        return value;
    }

    public void setGprmcValue(final String gprmcValue) {
        this.gprmcValue = gprmcValue;
        this.value = getValue(gprmcValue);
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    private double getValue(final String gprmcValue) {
        final int degrees = Integer.parseInt(gprmcValue.substring(0, 2));
        final double minutes = Double.parseDouble(gprmcValue.substring(2));

        return degrees * (type.equals(Type.N) ? 1 : -1) + minutes / MINUTES_IN_DEGREE;
    }

    /**
     * Latitude type (North or South).
     */
    public static enum Type {
        N, S
    }

    @Override
    public String toString() {
        return "GprmcLatitude{"
                + "gprmcValue=" + gprmcValue
                + ", type=" + type
                + '}';
    }

}
