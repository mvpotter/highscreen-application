package org.nmea.gprmc.geo;

/**
 * Longitude for using in GPRMC format.
 */
public class GprmcLongitude {

    public static final double MINUTES_IN_DEGREE = 60.0;
    public static final int DEGREE_NUMBERS_COUNT = 3;
    /**
     * GPRMC longitude value in "dddmm.mm" format.<br />
     * d - degrees<br />
     * m - minutes
     */
    private String gprmcValue;

    /**
     * Absolute value.<br />
     * > 0 -> West<br />
     * < 0 -> East
     */
    private double value;

    /**
     * Longitude type.<br />
     * <b>E</b> - east<br />
     * <b>W</b> - west
     */
    private Type type;

    public GprmcLongitude(final String gprmcValue, final Type type) {
        setType(type);
        setGprmcValue(gprmcValue);
    }

    public String getGprmcValue() {
        return gprmcValue;
    }

    public void setGprmcValue(final String gprmcValue) {
        this.gprmcValue = gprmcValue;
        value = getValue(gprmcValue);
    }

    public Type getType() {
        return type;
    }

    public void setType(final Type type) {
        this.type = type;
    }

    public double getValue() {
        return value;
    }

    private double getValue(final String gprmcValue) {
        final int degrees = Integer.parseInt(gprmcValue.substring(0, DEGREE_NUMBERS_COUNT));
        final double minutes = Double.parseDouble(gprmcValue.substring(DEGREE_NUMBERS_COUNT));

        return degrees * (type.equals(Type.E) ? 1 : -1) + minutes / MINUTES_IN_DEGREE;
    }

    /**
     * Longitude type (East or West).
     */
    public static enum Type {
        E, W
    }

    @Override
    public String toString() {
        return "GprmcLongitude{"
                + "gprmcValue=" + gprmcValue
                + ", type=" + type
                + '}';
    }

}
