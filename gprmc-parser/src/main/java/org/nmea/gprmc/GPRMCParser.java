package org.nmea.gprmc;

import org.highscreen.SystemException;
import org.nmea.Parser;
import org.nmea.gprmc.errors.ParserError;
import org.nmea.gprmc.geo.GprmcLatitude;
import org.nmea.gprmc.geo.GprmcLongitude;
import org.nmea.gprmc.validation.GprmcValidator;
import org.nmea.gprmc.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.nmea.gprmc.GprmcPacket.*;

/**
 * Parses GPRMC packets.
 * Packets must have the following format:
 *
 * Example: $GPRMC,225446,A,4916.45,N,12311.12,W,000.5,054.7,191194,020.3,E*68
 *
 * 225446       Time of fix 22:54:46 UTC
 * A            Navigation receiver warning A = OK, V = warning
 * 4916.45,N    GprmcLatitude 49 deg. 16.45 min North
 * 12311.12,W   GprmcLongitude 123 deg. 11.12 min West
 * 000.5        Speed over ground, Knots
 * 054.7        Course Made Good, True
 * 191194       Date of fix  19 November 1994
 * 020.3,E      Magnetic variation 20.3 deg East
 * *68          mandatory checksum
 *
 */
public class GprmcParser implements Parser<GprmcPacket> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GprmcParser.class);

    private Validator validator = new GprmcValidator();

    /**
     * Parses GPRMC string.
     *
     * @param string string in GPRMC format
     * @return GPRMC packet or null if string cannot be parsed
     *
     * @throws SystemException if error occurred
     */
    @Override
    public GprmcPacket parse(final String string) {
        LOGGER.debug(string);

        if (!validator.validate(string)) {
            throw new SystemException(ParserError.UNSUPPORTED_FORMAT);
        }

        final String[] tokensArray = string.split(SEPARATOR);
        return createGPRMCPacket(tokensArray);
    }

    private GprmcPacket createGPRMCPacket(final String[] tokensArray) {
        final GprmcPacket packet = new GprmcPacket();
        packet.setValidityType(ValidityType.valueOf(tokensArray[VALIDITY_TYPE_INDEX]));
        packet.setTimeStamp(GprmcUtils.getTimeStamp(tokensArray));
        packet.setGprmcLatitude(new GprmcLatitude(tokensArray[LATITUDE_INDEX],
                GprmcLatitude.Type.valueOf(tokensArray[LATITUDE_TYPE_INDEX])));
        packet.setGprmcLongitude(new GprmcLongitude(tokensArray[LONGITUDE_INDEX],
                GprmcLongitude.Type.valueOf(tokensArray[LONGITUDE_TYPE_INDEX])));
        packet.setSpeed(Double.parseDouble(tokensArray[SPEED_INDEX]));
        packet.setCourse(Double.parseDouble(tokensArray[COURSE_INDEX]));

        return packet;
    }

}
