package org.nmea.gprmc.validation;

import org.nmea.gprmc.GprmcPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Validator for GPRMC packet.
 */
public class GprmcValidator implements Validator {

    public static final int CHECKSUM_LENGTH = 3;
    public static final int NUMBER_OF_TOKENS_IN_GPRMC_STRING = 11;

    private static final Logger LOGGER = LoggerFactory.getLogger(GprmcValidator.class);

    public boolean validate(final String gprmcString) {
        final List<Validator> validators = new ArrayList<Validator>();
        validators.add(new StartKeyValidator());
        validators.add(new TimestampValidator());
        validators.add(new ValidityTypeValidator());
        validators.add(new LatitudeValidator());
        validators.add(new LatitudeTypeValidator());
        validators.add(new LongitudeValidator());
        validators.add(new LongitudeTypeValidator());
        validators.add(new SpeedValidator());
        validators.add(new CourseValidator());
        validators.add(new DateValidator());

        if (!gprmcString.contains(GprmcPacket.GPRMC_KEY)
            || gprmcString.charAt(gprmcString.length() - CHECKSUM_LENGTH) != GprmcPacket.CHECKSUM_DELIMITER) {
            return false;
        }
        final String[] tokensArray = gprmcString.split(GprmcPacket.SEPARATOR);
        if (tokensArray.length < NUMBER_OF_TOKENS_IN_GPRMC_STRING) {
            LOGGER.warn("The string has unsupported format");
            return false;
        }
        for (int i = 0; i < Math.min(tokensArray.length, validators.size()); i++) {
            if (!validators.get(i).validate(tokensArray[i])) {
                return false;
            }
        }

        return true;
    }

}
