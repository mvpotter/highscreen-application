package org.nmea.gprmc.validation;

import org.nmea.gprmc.GprmcPacket;

/**
 * Validates GPRMC packets's validity parameter.
 */
public class ValidityTypeValidator implements Validator {

    @Override
    public boolean validate(final String value) {
        boolean result = true;
        try {
            GprmcPacket.ValidityType.valueOf(value);
        } catch (IllegalArgumentException nfe) {
            result = false;
        }

        return result;
    }

}
