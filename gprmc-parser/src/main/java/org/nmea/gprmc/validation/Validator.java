package org.nmea.gprmc.validation;

/**
 * Data validator.
 */
public interface Validator {

    /**
     * Validates value.
     *
     * @param value string value
     * @return true if valid
     */
    boolean validate(final String value);

}
