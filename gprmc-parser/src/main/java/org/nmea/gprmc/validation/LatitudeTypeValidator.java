package org.nmea.gprmc.validation;

import org.nmea.gprmc.geo.GprmcLatitude;

/**
 * Validator for latitude type.
 */
public class LatitudeTypeValidator implements Validator {

    @Override
    public boolean validate(final String value) {
        boolean result = true;
        try {
            GprmcLatitude.Type.valueOf(value);
        } catch (IllegalArgumentException nfe) {
            result = false;
        }

        return result;
    }

}
