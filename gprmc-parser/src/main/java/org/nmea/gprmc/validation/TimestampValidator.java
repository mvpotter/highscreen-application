package org.nmea.gprmc.validation;

/**
 * Validator for timestamp.
 */
public class TimestampValidator extends DoubleValueValidator {

    public static final int TIME_STRING_LENGTH = 6;

    @Override
    public boolean validate(final String value) {
        boolean result = true;
        if (value.length() < TIME_STRING_LENGTH) {
            result = false;
        }
        if (!super.validate(value)) {
            result = false;
        }

        return result;
    }

}
