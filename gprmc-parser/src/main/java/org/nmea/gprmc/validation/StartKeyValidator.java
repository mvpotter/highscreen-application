package org.nmea.gprmc.validation;

import org.nmea.gprmc.GprmcPacket;

/**
 * Validator for GPRMC packet's start key.
 */
public class StartKeyValidator implements Validator {

    @Override
    public boolean validate(final String value) {
        boolean result = true;
        if (!value.equals(GprmcPacket.GPRMC_KEY)) {
            result = false;
        }

        return result;
    }

}
