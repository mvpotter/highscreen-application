package org.nmea.gprmc.validation;

import org.nmea.gprmc.geo.GprmcLongitude;

/**
 * Validator for longitude type.
 */
public class LongitudeTypeValidator implements Validator {

    @Override
    public boolean validate(final String value) {
        boolean result = true;
        try {
            GprmcLongitude.Type.valueOf(value);
        } catch (IllegalArgumentException nfe) {
            result = false;
        }

        return result;
    }

}
