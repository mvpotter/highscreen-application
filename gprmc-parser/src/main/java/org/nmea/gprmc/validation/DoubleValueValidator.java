package org.nmea.gprmc.validation;

/**
 * Validator for double value parameters.
 */
public class DoubleValueValidator implements Validator {

    @Override
    public boolean validate(final String value) {
        boolean result = true;
        try {
            Double.parseDouble(value);
        } catch (NumberFormatException nfe) {
            result = false;
        }

        return result;
    }

}
