package org.nmea.gprmc.validation;

/**
 * Validator for date parameter.
 */
public class DateValidator implements Validator {

    @Override
    public boolean validate(final String value) {
        boolean result = true;

        try {
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            result = false;
        }

        return result;
    }

}
