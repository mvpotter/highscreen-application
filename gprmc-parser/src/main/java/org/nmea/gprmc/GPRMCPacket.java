package org.nmea.gprmc;

import org.nmea.gprmc.geo.GprmcLatitude;
import org.nmea.gprmc.geo.GprmcLongitude;

import java.util.Date;

/**
 * GPRMC packet.
 *
 * Example: $GPRMC,225446,A,4916.45,N,12311.12,W,000.5,054.7,191194,020.3,E*68
 *
 * 225446       Time of fix 22:54:46 UTC
 * A            Navigation receiver warning A = OK, V = warning
 * 4916.45,N    gprmcLatitude 49 deg. 16.45 min North
 * 12311.12,W   gprmcLongitude 123 deg. 11.12 min West
 * 000.5        Speed over ground, Knots
 * 054.7        Course Made Good, True
 * 191194       Date of fix  19 November 1994
 * 020.3,E      Magnetic variation 20.3 deg East
 * *68          mandatory checksum
 */
public class GprmcPacket {

    public static final String GPRMC_KEY = "$GPRMC";
    public static final char CHECKSUM_DELIMITER = '*';
    public static final String SEPARATOR = ",";

    public static final String DATE_TIME_FORMAT = "ddMMyyhhmmss";

    public static final int TIME_INDEX = 1;
    public static final int VALIDITY_TYPE_INDEX = 2;
    public static final int LATITUDE_INDEX = 3;
    public static final int LATITUDE_TYPE_INDEX = 4;
    public static final int LONGITUDE_INDEX = 5;
    public static final int LONGITUDE_TYPE_INDEX = 6;
    public static final int SPEED_INDEX = 7;
    public static final int COURSE_INDEX = 8;
    public static final int DATE_INDEX = 9;

    /**
     * GPRMC packet timestamp.
     */
    private Date timeStamp;

    /**
     * Valid packet or not.<br />
     * <br />
     * <b>A</b> - valid<br />
     * <b>V</b> - not
     */
    private ValidityType validityType;

    /**
     * Latitude.
     */
    private GprmcLatitude gprmcLatitude;

    /**
     * Longitude.
     */
    private GprmcLongitude gprmcLongitude;

    /**
     * Speed in knots.<br />
     * (speed * 1.852) = speed in km/h
     */
    private double speed;

    /**
     * Course Made Good.
     */
    private double course;

    public GprmcPacket() {
    }

    public Date getTimeStamp() {
        return (Date) timeStamp.clone();
    }

    public void setTimeStamp(final Date timeStamp) {
        this.timeStamp = (Date) timeStamp.clone();
    }

    public boolean isValid() {
        return validityType.equals(ValidityType.A);
    }

    public ValidityType getValidityType() {
        return validityType;
    }

    public void setValidityType(final ValidityType validityType) {
        this.validityType = validityType;
    }

    public GprmcLatitude getGprmcLatitude() {
        return gprmcLatitude;
    }

    public void setGprmcLatitude(final GprmcLatitude gprmcLatitude) {
        this.gprmcLatitude = gprmcLatitude;
    }

    public GprmcLongitude getGprmcLongitude() {
        return gprmcLongitude;
    }

    public void setGprmcLongitude(final GprmcLongitude gprmcLongitude) {
        this.gprmcLongitude = gprmcLongitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(final double speed) {
        this.speed = speed;
    }

    public double getCourse() {
        return course;
    }

    public void setCourse(final double course) {
        this.course = course;
    }

    /**
     * Describes packet validity.<br />
     * A - valid<br />
     * V - not
     */
    public static enum ValidityType {
        A, V
    }

    @Override
    public String toString() {
        return "GprmcPacket{"
                + "timeStamp=" + timeStamp
                + ", validityType=" + validityType
                + ", gprmcLatitude=" + gprmcLatitude
                + ", gprmcLongitude=" + gprmcLongitude
                + ", speed=" + speed
                + ", course=" + course
                + '}';
    }
}
