package org.nmea.gprmc.errors;

import org.highscreen.ErrorCode;

/**
 * Enum describes parsing errors.
 */
public enum ParserError implements ErrorCode {
    UNSUPPORTED_FORMAT(1);

    private final int errorCode;

    private ParserError(final int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public int getNumber() {
        return errorCode;
    }
}
