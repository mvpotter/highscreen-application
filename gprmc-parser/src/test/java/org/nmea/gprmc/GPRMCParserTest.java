package org.nmea.gprmc;

import org.highscreen.SystemException;
import org.junit.Assert;
import org.junit.Test;
import org.nmea.gprmc.errors.ParserError;

import java.util.Locale;

public class GprmcParserTest {

    public static final String TEST_CORRECT_INPUT = "$GPRMC,003257.094,A,5451.1241,N,08305.8974,E,0.48,0.00,090513,,,A*6D";
    public static final String TEST_INCORRECT_INPUT = "003257.094,A,5451.1241,N,08305.8974,E,0.48,0.00,090513,,,A*6D";

    private GprmcParser gprmcParser = new GprmcParser();

    @Test
    public void testParseCorrectInput() {
        GprmcPacket gprmcPacket = gprmcParser.parse(TEST_CORRECT_INPUT);
        Assert.assertNotNull(gprmcPacket);

        String[] tokenArray = TEST_CORRECT_INPUT.split(",");

        Assert.assertEquals(GprmcUtils.getTimeStamp(tokenArray), gprmcPacket.getTimeStamp());
        Assert.assertEquals(tokenArray[2], gprmcPacket.getValidityType().toString());
        Assert.assertEquals(tokenArray[3], gprmcPacket.getGprmcLatitude().getGprmcValue());
        Assert.assertEquals(tokenArray[4], gprmcPacket.getGprmcLatitude().getType().toString());
        Assert.assertEquals(tokenArray[5], gprmcPacket.getGprmcLongitude().getGprmcValue());
        Assert.assertEquals(tokenArray[6], gprmcPacket.getGprmcLongitude().getType().toString());
        Assert.assertEquals(tokenArray[7], String.format(Locale.ENGLISH, "%.2f", gprmcPacket.getSpeed()));
        Assert.assertEquals(tokenArray[8], String.format(Locale.ENGLISH, "%.2f", gprmcPacket.getCourse()));

    }

    @Test
    public void testParseIncorrectInput() {
        try {
            gprmcParser.parse(TEST_INCORRECT_INPUT);
        } catch (SystemException exception) {
            Assert.assertEquals(ParserError.UNSUPPORTED_FORMAT, exception.getErrorCode());
        }
    }

}
