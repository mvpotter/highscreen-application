package org.nmea.gprmc.validation;

import org.junit.Assert;
import org.junit.Test;

public class GprmcValidatorTest {

    private static final String CORRECT_TEST_STRING = "$GPRMC,003445.000,A,5451.1270,N,08305.9121,E,6.49,108.44,090513,,,A*60";
    private static final String INCORRECT_TEST_STRING = "$GPRMC,003445.000,A,5451.1270,N,08305.9121,E,6.49,108.44,090513,,,";

    private Validator validator = new GprmcValidator();

    @Test
    public void testValidateCorrect() {
        Assert.assertTrue(validator.validate(CORRECT_TEST_STRING));
    }

    @Test
    public void testValidateIncorrect() {
        Assert.assertFalse(validator.validate(INCORRECT_TEST_STRING));
    }

}
